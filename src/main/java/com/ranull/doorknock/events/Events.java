package com.ranull.doorknock.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class Events implements Listener {
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if (!event.getPlayer().hasPermission("doorknock.knock")
				|| !event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
			return;
		}

		if (event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR)) {
			if (event.getClickedBlock().getType().equals(Material.OAK_DOOR)
					|| event.getClickedBlock().getType().equals(Material.SPRUCE_DOOR)
					|| event.getClickedBlock().getType().equals(Material.BIRCH_DOOR)
					|| event.getClickedBlock().getType().equals(Material.JUNGLE_DOOR)
					|| event.getClickedBlock().getType().equals(Material.ACACIA_DOOR)
					|| event.getClickedBlock().getType().equals(Material.DARK_OAK_DOOR)) {
				Location loc = event.getClickedBlock().getLocation();
				loc.getWorld().playSound(loc, Sound.ENTITY_ZOMBIE_ATTACK_WOODEN_DOOR, (float) 0.1, 1);
			}

			if (event.getClickedBlock().getType().equals(Material.IRON_DOOR)) {
				Location location = event.getClickedBlock().getLocation();

				location.getWorld().playSound(location, Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, (float) 0.1, 1);
			}
		}
	}
}
