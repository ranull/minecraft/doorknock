package com.ranull.doorknock;

import com.ranull.doorknock.events.Events;
import org.bukkit.plugin.java.JavaPlugin;

public class DoorKnock extends JavaPlugin {
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new Events(), this);
	}
}